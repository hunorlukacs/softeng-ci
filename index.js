const express = require('express'),
  morgan = require('morgan'),
  http = require('http'),
  bodyParser = require('body-parser');
const app = express();
const server = http.createServer(app);
app.use(morgan('tiny'));
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (_, res) => {
  res.sendFile('./page.html', { root: __dirname });
});


const PORT = process.env.PORT || 5000;
server.listen(PORT, () => console.log(`Server started on port ${PORT}`));
