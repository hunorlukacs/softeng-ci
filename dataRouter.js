
const express = require('express'),
  dataRepo = require('./dataRepository');

const router = express.Router();

router.get('/findall', (req, res) => {
  dataRepo.findAllData()
  .then((resData) => {
    res.status(200).json(resData);
  })
  .catch((err) => {
    console.error(new Error(`Error in findall: ${err}`));
    res.status(400).json('No data found!');
  })
});

router.post('/insert', (req, res) => {
  const { username } = req.body;
  const { password } = req.body;
  console.log(`insert: username: ${username}, password: ${password}`);
  dataRepo.insertData(username, password)
  .then(() => {
    res.status(200).json({msg: 'Insertion successful!'});
  })
  .catch((err) => {
    console.error(new Error(`Error in insertion: ${err}`));
    res.status(400).json({msg: 'Insertion failed!'});
  })
});

module.exports = router;