const execute = require('./config');

exports.findAllData = () => execute('SELECT * FROM data');
exports.insertData = (username, password) => execute(`
  INSERT INTO data VALUES(?, ?);
`, [username, password]);
