FROM node:12-alpine

RUN mkdir -p /usr/softeng

WORKDIR /usr/softeng

COPY . .

RUN npm install

ENV PORT=5000

EXPOSE 5000

CMD node index.js
