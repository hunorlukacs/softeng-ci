
const mysql = require('mysql');

const pool = mysql.createPool({
  connectionLimit: 3,
  multipleStatements: true,
  database: 'softengdb',
  port: 3306,
  host: 'db',
  user: 'root',
  password: 'root',
});

module.exports = (query, options = []) => new Promise((resolve, reject) => {
  pool.query(query, options, (error, results) => {
    if (error) {
      reject(new Error(`Error while running '${query}: ${error}'`));
    }
    resolve(results);
  });
});
